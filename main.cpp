/*
 * author Nikolay Russkin <nrusskin@gmail.com
 */
#include <iostream>
#include <string.h>
#include <stdio.h>

using namespace std;

#define MAX_CHORE_SIZE 256

enum Menu {
	MENU_INVALID= 0,
	MENU_ADD	= 1,
	MENU_COUNT	= 2,
	MENU_PRINT	= 3,
	MENU_DELETE	= 4,
	MENU_EXIT	= 5,
	MENU_SIZE	= 6
};

class List
{
protected:
	struct Item {
		Item(const char* _value, Item* _next) : next(_next) {
			size_t len = strlen(_value);
			value = new char [strlen(_value)];
			strncpy(value, _value, len-1);
		}
		~Item() {
			delete [] value;
		}

		char* value;
		Item* next;
	};

	Item*	chores;
	size_t	_size;

public:
	List() : chores(NULL), _size(0) {
	}

	~List() {
		Item* item = chores;
		while (item) {
			Item* del = item;
			item = item->next;
			delete del;
		}
	}

	void print() const {
		Item* item = chores;
		size_t idx = 0;
		while (item) {
			cout << ">" << ++idx << ". " << item->value << endl;
			item = item->next;
		}
	}

	size_t size() const {
		return _size;
	}

	void add(const char* chore) {
		chores = new Item(chore, chores);
		++_size;
	}

	void del(size_t index) {
		Item *prev = chores, *item = chores;

		while (--index) {
			prev = item;
			item = item->next;
		}

		if (item == chores)
			chores = chores->next;
		else
			prev->next = item->next;

		delete item;
		--_size;
	}
};


void getMenuChoice(Menu &Choice) {
        int input = MENU_INVALID;
        cout << "Enter choice: ";
        cin >> input;
        while ( !(MENU_INVALID < input && MENU_SIZE > input))
        {
                cout << "Invalid choice.  Please re-enter: ";
                cin >> input;
        }
        Choice = Menu(input);
}

void displayMenu() {
	cout<<"----MENU----" << endl
		<< "1. Add item" << endl
		<< "2. How many chores are in the list?" << endl
		<< "3. Print list of chores" << endl
		<< "4. Delete an item" << endl
		<< "5. Exit" << endl;
}

void addItem(List& chores) {
	char chore[MAX_CHORE_SIZE];
	memset(chore, 0, MAX_CHORE_SIZE);
	cout << "Enter new chore: ";
	getchar();
	fgets(chore, MAX_CHORE_SIZE, stdin);
	chores.add(chore);
}

void printCoresCount(const List& chores) {
	cout << ">Chores list size: " << chores.size() << endl;
}

void printChores(const List& chores) {
	chores.print();
}

void deleteChore(List& chores) {
	if (chores.size() == 0) {
		cout << ">List is empty" << endl;
		return;
	}

	size_t idx;
	chores.print();
	cout << "Delete chore: ";
	cin >> idx;
	while (idx > chores.size() || idx < 1) {
		cout << "Invalid choice.  Please re-enter: ";
		cin >> idx;
	}
	chores.del(idx);
}

int processMenuChoice(const Menu Choice, List& chores) {
        switch(Choice) {
        case MENU_ADD:
                addItem(chores);
                break;
        case MENU_COUNT:
                printCoresCount(chores);
                break;
        case MENU_PRINT:
				printChores(chores);
                break;
		case MENU_DELETE:
				deleteChore(chores);
				break;
        case MENU_EXIT:
                return 1;
        default:
                break;
        }
        return 0;
}
int main() {
	List	chores;
	Menu	choice;
	
	do {
		displayMenu();
		getMenuChoice(choice);
	} while (!processMenuChoice(choice, chores));

	return 0;
}
